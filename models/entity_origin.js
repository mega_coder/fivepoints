var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var entityOriginSchema = Schema({
    name: { type: String }
}, { collection: "entityOriginCustom"});

module.exports = mongoose.model("EntityOrigin", entityOriginSchema);