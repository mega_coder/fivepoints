var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var entitySchema = Schema({
    start: { type: Number },
    end: { type: Number },
    value: { type: String },
    entity: {type: Schema.Types.ObjectId, ref: 'EntityOrigin' }  
}, { collection: "entityCustom" });

module.exports = mongoose.model("Entity", entitySchema);