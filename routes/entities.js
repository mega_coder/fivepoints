var express = require('express');
var router = express.Router();
var Entity = require('../models/entity');
var mongoose = require('mongoose');

router.get('/', function (req, res, next) {
    Entity.find().populate("entity").exec(function (err, entities) {
        if (err) {
            res.send(err);
        }
        if (!entities) {
            res.status(404).send();
        }
        else {
            return res.json(entities);
        }
    });
});

router.post('/', function (req, res, next) {

    var entity = new Entity(req.body);
    entity.entity = mongoose.Types.ObjectId(req.body.entity);
    entity.save(function (err, insertedEntity) {
        if (err) {
            res.send(err);
        }
        console.log(insertedEntity);
        res.json(insertedEntity);

    });
});

module.exports = router;
