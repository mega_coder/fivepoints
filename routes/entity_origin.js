var express = require('express');
var router = express.Router();
var EntityOrigin = require('../models/entity_origin');

router.get('/', function (req, res, next) {
    EntityOrigin.find().exec(function (err, entities_origins) {
        if (err) {
            res.send(err);
        }
        if (!entities_origins) {
            res.status(404).send();
        }
        else {
            return res.json(entities_origins);
        }
    });
});

router.post('/', function (req, res, next) {
    var entityOrigin = new EntityOrigin(req.body);
    entityOrigin.save(function (err, insertedEntityOrigin) {
        if (err) {
            res.send(err);
        }
        console.log(insertedEntityOrigin);
        res.json(insertedEntityOrigin);
    });
});

module.exports = router;