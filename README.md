# Chatbot-admin-panel-backend

Ce projet a été géneré avec [Express.js Generator](https://github.com/expressjs/generator).

## Informations utiles

Toutes les routes de web services REST se trouvent dans le dossier 'routes'.  

#### Routes du web service des 'actions'

Route globale : `/actions`  
`/actions` `GET` : Retourne toutes les actions existantes.  
`/actions` `POST` : Création d'une nouvelle action.  

#### Routes du web service des 'domains'

Route globale : `/domains`  
`/domains` `GET` : Retourne tous les domaines existants.  
`/domains` `POST` : Création d'un nouveau domaine.  

#### Routes du web service des 'intents'

Route globale : `/intents`  
`/intents` `GET` : Retourner toutes les intentions existantes.  
`/intents` `POST` : Création d'une nouvelle intention et l'affecter a un domaine et une action.    
`/intents/:_id` `DELETE` : Suppression d'une intention ainsi que tous les exemples liés.    
`/intents/getIntentById/:id` `GET` : Retourner une intention par son identifiant.  
`/intents/updateIntent/:_id` `PUT` : Modification d'une intention, son domaine et son action.  
`/intents/pushEntitiesToIntent/:_id` `PUT` : Ajouter des entitées a une intention existantes.  

#### Routes du web service des 'entities'

Route globale : `/entities`  
`/entities` `GET` : Retourne toutes les entités existantes.  
`/entities` `POST` : Création d'une nouvelle entité.  

#### Routes du web service des 'examples'

Route globale : `/examples`  
`/examples` `GET` : Retourne tous les exemples existants avec leur intention et leurs entitées.  
`/examples` `POST` : Création d'un nouveau exemple et l'affecter à une intention, domaine, et action déjà existants + création d'une ou plusieurs entitées/réponses de chatbot puis les affecter au exemple à créer.  
`/examples/getExamplesByIntent/:_id` `GET` : Retourner tous les exemples par une intention précise.  
`/examples/:_id` `DELETE` : Supprimer un exemple par son id.  
`/examples/deleteAllExamplesByIntent/:_id` `DELETE` : Supprimer tous les exemples par intention.  
`/examples/updateExample/:_id` `PUT` : Mettre à jour un exemple.  
`/examples/insertJson/:_id` `POST` : Ajouter des données en masse à partir d'un fichier JSON.  

